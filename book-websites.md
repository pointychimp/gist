# Copied from [here](http://www.reddit.com/r/trackers/comments/hrgmv/tracker_with_pdfsebooks_of_college_textbooks/c1xrq44)

To convert your eBooks to any other format (maybe to get it to work on your e-reader), use Calibre. (google around for other Calibre alternatives and I can add them here if you'd like)

General public ebook/textbook resources

	http://gen.lib.rus.ec (Use a proxy if you are having issues connecting)
	http://textbooknova.com
	http://en.bookfi.org/
	http://www.gutenberg.org
	http://ebookee.org
	http://www.manybooks.net
	http://www.giuciao.com
	http://www.feedurbrain.com
	http://oll.libertyfund.org/index.php?option=com_content&task=view&id=380
	http://www.alleng.ru/ russian site
	http://www.eknigu.com/ russian site
	http://ishare.iask.sina.com.cn/
	http://2020ok.com/
	http://www.freebookspot.es/Default.aspx
	http://www.freeetextbooks.com/ need to signup
	http://onebigtorrent.org/
	http://www.downeu.me/ebook/
	http://forums.mvgroup.org (need to register)
	http://theaudiobookbay.com/ (audiobooks)

Here's a custom search engine for ebooks: http://www.google.com/cse/home?cx=000661023013169144559:a1-kkiboeco

Sites it indexes:

    http://gen.lib.rus.ec/*
    http://ebookee.org/*
    http://ebooksbay.org/*
    http://free-books.us.to/*
    http://librarypirate.me/*
    http://textbooknova.com/*
    http://www.downeu.com/*
    http://ebookshare.net/*
    http://www.freebookspot.es/*
    http://www.demonoid.me/*
    http://www.kat.ph/*
    http://www.esnips.com/*
    www.4shared.com/*
    http://www.ebooklink.net/*
    http://wowebook.net/*
    http://www.pdfchm.net/*
    http://www.free-ebook-download.net/*
    http://ebookbrowse.com/*
    http://www.ebook3000.com/*
    http://www.ipmart-forum.com/*
    http://www.mediafire.com/*

Academic Torrents (new site)

	http://academictorrents.com

Public Trackers

	http://thepiratebay.se/browse/601
	http://www.kat.ph/books/
	http://bitsnoop.com/browse/other-ebooks/
	http://www.filestube.com

There are a lot more public torrent trackers, see here for more.

These last few are sort of specialized eBook private trackers, google them for some info.

	http://thegeeks.bz
	http://theplace.bz
	http://thevault.bz
	http://bitseduce.com

You can also try googling phrases like 'textbook/book title .torrent' or 'textbook/book title .pdf' or 'textbook/book title ebook' or anything along those lines if you are getting desperate, but beware of malware, viruses, etc.

Private ebook trackers (Feel free to make a request for any of these in the Consolidated Invite Thread (see the no movement list in the OP)

	http://bibliotik.org
	http://bitme.org
	http://myanonamouse.net (see reddit offers for invite)
	http://bitspyder.net
	http://www.learnbits.me
	http://www.ebookvortex.com
	http://elbitz.net
	http://docspedia.org
	http://abtorrents.com (audiobooks)

General private trackers have some stuff too, try places like TL or DH, etc, etc. Reddit's unofficial private tracker BaconBits (see /r/baconbits for more info) has a nice collection of eBooks as well. Better than most other private trackers actually.

Don't forget that What.CD and Waffles.FM! They both have a huge selection of eBooks.

IRC

	irc://irc.undernet.org/bookz
	irc://irc.irchighway.net/ebooks

If you don't know how to use IRC to download books, check out this link or read this:

IRC

You obviously need an IRC client installed, a quick google search should find you a bunch of decent free ones.

Load the client and then type:

	/server irc.irchighway.net

This will connect you to the irchighway server. Once you're on there, type:

	/join #ebooks

This will take you into the ebooks channel, which has more than 2.2Terabytes of ebooks for free download. Once you're in, all you have to do to find what you want is to simply type:

	@search "Author or book title"



... removing the inverted commas.

You'll get a message saying that your search has been accepted. A few seconds later a searchbot will offer you a file, so accept the download. That will send you a .txt file that's zipped. Extract that and you'll get a list of files. I searched for Lian Hearn and got a list of 147 files. Here's an extract from it:

	!bald Lian Hearn - [Tales Of The Otori 02] - Grass For His Pillow .rar - 142.1 KB
	!bald Lian Hearn - Tales of the Otori 2 - Grass for His Pillow (CO.rar - 355.3 KB
	!bald Lian Hearn - Tales of the Otori 2 - Grass for His Pillow (ht.rar - 140.54 KB
	!bald Lian_Hearn_-_[Otori_01]_-_Across_The_Nightingale_Floor_(v3)_.rar - 111.62 KB
	!bald Lian_Hearn_-_[Otori_01]_-_Across_the_Nightingale_Floor_(v4.0.rar - 115.45 KB
	!bald Lian Hearn - [Tales Of The Otori 03] - Brilliance Of The Moo.rar - 175.53 KB
	!bald Lian Hearn - [Tales Of The Otori 04] - The Harsh Cry of the Heron (v1.0) (html).rar - 351.80 KB
	!bald Lian Hearn - [Tales Of the Otori 05] - Heaven's Net Is Wide (v5.0) (epub).rar - 588.40 KB

Say I wanted to download the first book. I'd copy the text shown below, deliberately removing the file size information (I've been booted for that before ):

	!bald Lian Hearn - [Tales Of The Otori 02] - Grass For His Pillow .rar

Then just paste that into the IRC window then press enter. After 2-3 seconds you get a prompt asking you to accept or decline the file.You know how to do the rest.

Then there are a few .i2p and .onion sites that have ebooks, not sure of the specifics - but I can get them when I have some time.

If I'm missing anything, PM me and I'll add it.